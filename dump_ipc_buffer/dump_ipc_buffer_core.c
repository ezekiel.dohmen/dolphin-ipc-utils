
//advligorts
#include "commData3.h"
#include "util/timing.h"
#include "drv/gpstime/gpstime_kernel.h"
#include "drv/rts-cpu-isolator/rts-cpu-isolator.h"

//LIGO Netlink Message Definitions
#include "../dolphin_daemon/include/daemon_messages.h"

#define RTS_LOG_PREFIX "find_active_ipcs"
#include "drv/rts-logger.h"


#include "hist.h"


#include <linux/module.h>       /* Needed by all modules */
#include <linux/kernel.h>       /* Needed for KERN_INFO */
#include <linux/kthread.h>       /* Needed for KERN_INFO */
#include <asm/cacheflush.h>
#include <asm/uaccess.h>
#include <asm/delay.h>
#include <linux/proc_fs.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/string.h>
#include <linux/timer.h>
#include <linux/ctype.h>

//Netlink Socket Headers
#include <net/sock.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>




//timing out and signaling an error
#define DAEMON_RESP_TIMEOUT_MS 2000
#define DAEMON_RESP_CHECK_RATE_MS 30

//#define NUM_IPCS 64


//These define what dolphin adapters are connected
//to the three dolphin networks 
static int ADAPTER_NUM = 0;
module_param(ADAPTER_NUM, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(ADAPTER_NUM, "The adapter we should use");

static int SEGMENT_ID = 1;
module_param(SEGMENT_ID, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(SEGMENT_ID, "The segment id of the shmem we are going to dump from");

static int RFM_NUM = 0;// 0 -> X leg, 1 -> Y leg, else PCIE interpretation
module_param(RFM_NUM, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(RFM_NUM, "The RFM network number, 0 = EX, 1 = EY");

static int IPC_NUM = 0;
module_param(IPC_NUM, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(IPC_NUM, "The ipc you want to dump");


//Dolphin adapter lookup, this is used to always order the indexes in a known order 
static volatile CDS_IPC_COMMS *g_dolphin_read_addr;
static volatile CDS_IPC_COMMS *g_dolphin_write_addr;

//Dolphin Daemon Messaging Globals
static atomic_t g_dolphin_init_success = ATOMIC_INIT(0);
static struct sock * g_nl_sock = NULL;
static int g_dolphin_bad_init = 1;



// End of globals *********************************************************


CDS_IPC_XMIT ipc_store[IPC_BLOCKS];


void print_active( void )
{

    int ipc_index;

    msleep(3000);

    for(ipc_index=0; ipc_index < IPC_BLOCKS; ++ipc_index)
    {
        ipc_store[ipc_index] = g_dolphin_read_addr->dBlock[ipc_index][IPC_NUM];
    }


    for(ipc_index=0; ipc_index < IPC_BLOCKS; ++ipc_index)
    {
        //If the value changed, it is active
	    int time = (g_dolphin_read_addr->dBlock[ipc_index][IPC_NUM].timestamp >> 32);
	    int cycle = g_dolphin_read_addr->dBlock[ipc_index][IPC_NUM].timestamp & 0xFFFFFFFF;
            RTSLOG_INFO("IPC index %d, cur timestamp: %d, cycle: %d\n", 
			ipc_index, time, cycle );
    }



}


// ************************************************************************
// Dolphin NIC initialization functions.
// ************************************************************************

//
// Callback for the netlink unicast response message
//
static void netlink_recv_resp(struct sk_buff *skb)
{
    struct nlmsghdr *nlh;
    int pid;
    all_dolphin_daemon_msgs_union any_msg;

    //Store info and pointer to message
    nlh = (struct nlmsghdr *)skb->data;
    pid = nlh->nlmsg_pid; // pid of sending process
    any_msg.as_hdr = (dolphin_mc_header *) nlmsg_data(nlh);


    //Make sure we got (at least) a good header, before we dive into message
    if ( !check_mc_header_valid(any_msg.as_hdr, nlmsg_len(nlh)) )
    {
        RTSLOG_ERROR("netlink_test_recv_msg() - Malformed message, too short or bad preamble\n");
        atomic_set(&g_dolphin_init_success, -1);
        return;
    }

    if(any_msg.as_hdr->interface_id != RT_KM_INTERFACE_ID)
    {
        RTSLOG_ERROR("netlink_test_recv_msg() - Got a message that had an interface_id for somthing else.\n");
        atomic_set(&g_dolphin_init_success, -1);
        return; //Msg not ment for us
    }

    switch(any_msg.as_hdr->msg_id)
    {
        case DOLPHIN_DAEMON_ALLOC_RESP:
            if ( !check_alloc_resp_valid(any_msg.as_alloc_resp, nlmsg_len(nlh)) )
            {
                RTSLOG_ERROR("netlink_test_recv_msg() - Got a DOLPHIN_DAEMON_ALLOC_RESP, but message was not valid, discarding. "
                        "Size was %u\n", nlmsg_len(nlh));
                atomic_set(&g_dolphin_init_success, -1);
                return;
            }

            if ( any_msg.as_alloc_resp->status != DOLPHIN_ERROR_OK )
            {
                RTSLOG_ERROR("netlink_test_recv_msg() - An error (%d) was returned by the dolphin daemon. Failed to init dolphin.\n", any_msg.as_alloc_resp->status);
                atomic_set(&g_dolphin_init_success, -1);
                return;
            }

            if( any_msg.as_alloc_resp->num_addrs != 1 )
            {
                RTSLOG_ERROR("netlink_test_recv_msg() - The number of returned addrs was %u, "
                             "but we requested %u segments.\n",
                             any_msg.as_alloc_resp->num_addrs,
                             1);
                atomic_set(&g_dolphin_init_success, -1);
                return;
            }

            //Loop over response and store pointers 
            //TODO: We ASSUME the order returned is the same as the order requested, this is correct, but we might want to return the adapter num in the future
            char * read_addr_tmp;
            char * write_addr_tmp;
            int offset;
            for(int i=0; i < any_msg.as_alloc_resp->num_addrs; ++i)
            {
                read_addr_tmp = (char*)any_msg.as_alloc_resp->addrs[ i ].read_addr;
                write_addr_tmp = (char*)any_msg.as_alloc_resp->addrs[ i ].write_addr;

                if(RFM_NUM == 0) offset = IPC_PCIE_BASE_OFFSET + RFM0_OFFSET;
                else if (RFM_NUM == 1) offset = IPC_PCIE_BASE_OFFSET + RFM1_OFFSET;
                else offset = IPC_PCIE_BASE_OFFSET;

                g_dolphin_read_addr = (CDS_IPC_COMMS *)(read_addr_tmp + offset);
                g_dolphin_write_addr = (CDS_IPC_COMMS *)(write_addr_tmp + offset);
            }

        break;

        default:
            RTSLOG_ERROR("netlink_test_recv_msg() - Got a message with ID %u, which we don't support, discarding.\n", any_msg.as_hdr->msg_id);
            atomic_set(&g_dolphin_init_success, -1);
            return;
        break;

    }
    atomic_set(&g_dolphin_init_success, 1);//Signal init was successful

}


int init_dolphin( void ) 
{
    struct sk_buff *skb_out;
    struct nlmsghdr *nlh;
    int res;
    dolphin_mc_alloc_req * alloc_req;

    struct netlink_kernel_cfg cfg = {
        .input = netlink_recv_resp,
    };

    g_nl_sock = netlink_kernel_create(&init_net, DOLPHIN_DAEMON_REQ_LINK_ID, &cfg);
    if (!g_nl_sock) {
        RTSLOG_ERROR("init_dolphin() - Error creating netlink socket.\n");
        return -1;
    }


    //Start message creation
    unsigned total_payload_sz = GET_ALLOC_REQ_SZ( 1 );

    ///Allocate the request's msg space, OS does free once it is sent
    skb_out = nlmsg_new( total_payload_sz, 0);
    if (!skb_out) {
        RTSLOG_ERROR("init_dolphin() - Failed to allocate skb for allocation request. Not sending...\n");
        return -1;
    }

    //Fill allocation request
    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, total_payload_sz, 0);
    alloc_req = (dolphin_mc_alloc_req*) nlmsg_data(nlh);
    alloc_req->header.msg_id = DOLPHIN_DAEMON_ALLOC_REQ;
    alloc_req->header.preamble_seq = DOLPHIN_DAEMON_MSG_PREAMBLE;
    alloc_req->num_segments = 1;
    //The RFMX uses the same segment ID across networks for RFM IPCs
    alloc_req->segments[0].segment_id = SEGMENT_ID;
    //Each connection uses a diffrent dolphin adapter, and we bridge the network across them
    alloc_req->segments[0].adapter_num = ADAPTER_NUM;
    alloc_req->segments[0].segment_sz_bytes = IPC_TOTAL_ALLOC_SIZE;

    res = nlmsg_multicast(g_nl_sock, skb_out, 0, DOLPHIN_DAEMON_REQ_GRP_ID, GFP_KERNEL);
    if( res != 0 )
    {
        if( res == -ESRCH)
            RTSLOG_ERROR("init_dolphin() - nlmsg_multicast() failed... Is the dolphin_daemon running?\n");
        else
            RTSLOG_ERROR("init_dolphin() - nlmsg_multicast() returned an error: %d\n", res);
        return -1;
    }

    //We wait for response to be handled by netlink_recv_resp()
    int time_waited_ms = 0;
    while( atomic_read(&g_dolphin_init_success) == 0 && time_waited_ms < DAEMON_RESP_TIMEOUT_MS)
    {
        msleep( DAEMON_RESP_CHECK_RATE_MS );
        time_waited_ms += DAEMON_RESP_CHECK_RATE_MS;
    }

    //Timeout case
    if(atomic_read(&g_dolphin_init_success) == 0)
    {
        RTSLOG_ERROR("init_dolphin() - Did not get a response from the dolphin_daemon in a timely manner.");
        return -1;
    }

    //If there was a returned error
    if(atomic_read(&g_dolphin_init_success) < 0)
    {
        RTSLOG_ERROR("init_dolphin() - dolphin_proxy_km returned an error, check dmesg output for error message\n");
        return -1;
    }

    //Otherwise globals are set and we should be good to go
    g_dolphin_bad_init = 0;
    return 0;


}

// ************************************************************************
void finish_dolphin( void ) 
{
    struct sk_buff *skb_out;
    struct nlmsghdr *nlh;
    int res;
    dolphin_mc_free_all_req * free_req;

    unsigned total_payload_sz = sizeof( dolphin_mc_free_all_req );

    if( g_nl_sock == NULL) return; //If we never set up the socket, just return

    //When this is set the dolphin_init failed, so we don't send the cleanup message
    if( g_dolphin_bad_init)
    {
        netlink_kernel_release(g_nl_sock);
        return;
    }


    //Allocate the request's msg space, OS does free once it's sent
    skb_out = nlmsg_new( total_payload_sz, 0);
    if (!skb_out) {
        RTSLOG_ERROR("finish_dolphin() - Failed to allocate skb for free request. Not sending...\n");
        return;
    }

    //Allocate and send message
    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, total_payload_sz, 0);


    free_req = (dolphin_mc_free_all_req*) nlmsg_data(nlh);
    free_req->header.msg_id = DOLPHIN_DAEMON_FREE_REQ;
    free_req->header.preamble_seq = DOLPHIN_DAEMON_MSG_PREAMBLE;

    res = nlmsg_multicast(g_nl_sock, skb_out, 0, DOLPHIN_DAEMON_REQ_GRP_ID, GFP_KERNEL);
    if( res != 0 )
        RTSLOG_ERROR("finish_dolphin() - nlmsg_multicast() returned an error: %d\n", res);

    netlink_kernel_release(g_nl_sock);
}

static void lr_switch_exit(void)
{
    RTSLOG_INFO("Goodbye, cdsrfmswitch is shutting down\n");

    // Cleanup the dolphin NIC connections
    finish_dolphin();
}



static int __init lr_switch_init(void)
{
    int error;
  
   /* 
    if(RFM_NUM != 0 && RFM_NUM != 1)
    {
        RTSLOG_ERROR("init: RFM_NUM not valid.\n");
	    return -1;
    }*/

    // Initialize Dolphin NICs and get data pointers.
    if ( init_dolphin() != 0 )
    {
        RTSLOG_ERROR("lr_switch_init() - Dolphin init failed, km exiting...\n");
        finish_dolphin();
        return -ENXIO;
    }


    print_active();
    lr_switch_exit();

    return -1;
}


module_init(lr_switch_init);
module_exit(lr_switch_exit);

MODULE_LICENSE("Dual BSD/GPL");
