
//advligorts
#include "commData3.h"
#include "util/timing.h"
#include "drv/gpstime/gpstime_kernel.h"
#include "drv/rts-cpu-isolator/rts-cpu-isolator.h"

//LIGO Netlink Message Definitions
#include "../dolphin_daemon/include/daemon_messages.h"

#define RTS_LOG_PREFIX "ipc_timing"
#include "drv/rts-logger.h"


#include "hist.h"


#include <linux/module.h>       /* Needed by all modules */
#include <linux/kernel.h>       /* Needed for KERN_INFO */
#include <linux/kthread.h>       /* Needed for KERN_INFO */
#include <asm/cacheflush.h>
#include <asm/uaccess.h>
#include <asm/delay.h>
#include <linux/proc_fs.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/string.h>
#include <linux/timer.h>
#include <linux/ctype.h>

//Netlink Socket Headers
#include <net/sock.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>




//timing out and signaling an error
#define DAEMON_RESP_TIMEOUT_MS 2000
#define DAEMON_RESP_CHECK_RATE_MS 30


//These define what dolphin adapters are connected
//to the three dolphin networks 
static int ADAPTER_NUM = 0;
module_param(ADAPTER_NUM, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(ADAPTER_NUM, "The adapter we should use");

static int SEGMENT_ID = 1;
module_param(SEGMENT_ID, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(SEGMENT_ID, "The segment id of the shmem we are going to dump from");

static int IPC_INDEX = 0;
module_param(IPC_INDEX, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(IPC_INDEX, "The index of the IPC to dump");

static int RFM_NUM = 0;
module_param(RFM_NUM, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(RFM_NUM, "The RFM network number, 0 = EX, 1 = EY");

static int STATS_INTERVAL_SEC = 10;
module_param(STATS_INTERVAL_SEC, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(RFM_NUM, "The time we should collect avg, max stats and report");

//Dolphin adapter lookup, this is used to always order the indexes in a known order 
static volatile CDS_IPC_COMMS *g_dolphin_read_addr;
static volatile CDS_IPC_COMMS *g_dolphin_write_addr;

//Dolphin Daemon Messaging Globals
static atomic_t g_dolphin_init_success = ATOMIC_INIT(0);
static struct sock * g_nl_sock = NULL;
static int g_dolphin_bad_init = 1;

static int g_core_used = -1;

static volatile int stop_working_threads;
atomic_t g_atom_has_exited = ATOMIC_INIT(0);


// End of globals *********************************************************



//
// This function gets the current time from the GPS driver
inline unsigned long my_current_time(void) {
    LIGO_TIMESPEC t;
    ligo_gpstime_get_ts(&t);
    return t.tv_sec;
}

int find_latest(int ipc_num)
{
    int block=0;
    int latest_block_index = 0;
    unsigned long latest_timestamp = g_dolphin_read_addr->dBlock[0][ipc_num].timestamp;

    for(block = 1; block < IPC_BLOCKS; block+=4)
    {
       if(g_dolphin_read_addr->dBlock[block][ipc_num].timestamp > latest_timestamp)
       {
           latest_block_index = block;
           latest_timestamp = g_dolphin_read_addr->dBlock[block][ipc_num].timestamp;
       }
    }


    RTSLOG_INFO("Latest block found index: %d, timestamp: %lu, cycle %lu\n", latest_block_index, (latest_timestamp>>32), latest_timestamp&0xFFFFFFFF);
    //unsigned long n = g_dolphin_read_addr->dBlock[ (latest_block_index+4)%IPC_BLOCKS  ][ipc_num].timestamp;
    //RTSLOG_INFO("Next block : timestamp: %lu, cycle %lu\n", (n>>32), n&0xFFFFFFFF);
    
    if((latest_timestamp>>32) == 0) return -1; //No time, no data

    return latest_block_index;
}

int wait_for_next_block(int ipc_num, int latest_block, uint64_t * wait_time_ns)
{

    uint64_t local_timer;
    timer_start(&local_timer);
    timer_start(wait_time_ns);
    int next_block = (latest_block+4)%IPC_BLOCKS;


    while( g_dolphin_read_addr->dBlock[next_block][ipc_num].timestamp < 
           g_dolphin_read_addr->dBlock[latest_block][ipc_num].timestamp)
    {
        if( timer_tock_ns(&local_timer) > 5000000) break; //Dont wait longer than 5000 us
    }
    timer_end_ns(wait_time_ns);
    return next_block;
}

void rt_loop( void )
{
    int latest_block;
    uint64_t time_ns, max_ns=0, loops=0, total_max_ns=0, stats_start_time=0;
    hist_context_t * hist_ctx;
    int64_t ranges [] = {45000, 55000, 60000, 62000, 65000, 67000, 70000, 75000, 80000, 90000, 100000, 150000};

    latest_block = find_latest(IPC_INDEX);
    if(latest_block == -1)
    {
	RTSLOG_ERROR("Did not find valid IPCs in the configured buffer. Exiting.\n");
	atomic_set(&g_atom_has_exited, 1);
	return;
    }
    latest_block = (latest_block+4)%IPC_BLOCKS;

    hist_ctx = hist_initialize(sizeof(ranges)/sizeof(ranges[0]), ranges);
    if(hist_ctx == NULL)
    {
        RTSLOG_INFO("The hist allocation failed...\n");
        atomic_set(&g_atom_has_exited, 1);
        return;
    }


    while(!stop_working_threads) {
        latest_block = wait_for_next_block(IPC_INDEX, latest_block, &time_ns);

	    if(loops > (1<<14) )
            {
                hist_add_element(hist_ctx, time_ns);
                if (time_ns > total_max_ns) total_max_ns = time_ns;
		if (time_ns > max_ns) max_ns = time_ns;

		if(stats_start_time == 0)
		{
			timer_start(&stats_start_time);
		}
		if(timer_tock_ns(&stats_start_time)/((uint64_t)1e9) > STATS_INTERVAL_SEC)
		{
			RTSLOG_INFO("Max in last %d seconds was %llu.\n", STATS_INTERVAL_SEC, max_ns);
			max_ns = 0;
			stats_start_time = 0;
		}	

            }

            if( time_ns > 80000)
            {
                RTSLOG_ERROR("At %lu, it took %llu ns to get the next IPC...\n", my_current_time(), time_ns);
            }
	    ++loops;
    }

    RTSLOG_INFO("Last time: %llu, max time: %llu\n", time_ns, total_max_ns);
    RTSLOG_INFO("Histogram Of All Latencies (ns)\n");
    udelay(19999);
    hist_print_stats(hist_ctx);

    hist_free(hist_ctx);

    atomic_set(&g_atom_has_exited, 1);
}




// ************************************************************************
// Dolphin NIC initialization functions.
// ************************************************************************

//
// Callback for the netlink unicast response message
//
static void netlink_recv_resp(struct sk_buff *skb)
{
    struct nlmsghdr *nlh;
    int pid;
    all_dolphin_daemon_msgs_union any_msg;

    //Store info and pointer to message
    nlh = (struct nlmsghdr *)skb->data;
    pid = nlh->nlmsg_pid; // pid of sending process
    any_msg.as_hdr = (dolphin_mc_header *) nlmsg_data(nlh);


    //Make sure we got (at least) a good header, before we dive into message
    if ( !check_mc_header_valid(any_msg.as_hdr, nlmsg_len(nlh)) )
    {
        RTSLOG_ERROR("netlink_test_recv_msg() - Malformed message, too short or bad preamble\n");
        atomic_set(&g_dolphin_init_success, -1);
        return;
    }

    if(any_msg.as_hdr->interface_id != RT_KM_INTERFACE_ID)
    {
        RTSLOG_ERROR("netlink_test_recv_msg() - Got a message that had an interface_id for somthing else.\n");
        atomic_set(&g_dolphin_init_success, -1);
        return; //Msg not ment for us
    }

    switch(any_msg.as_hdr->msg_id)
    {
        case DOLPHIN_DAEMON_ALLOC_RESP:
            if ( !check_alloc_resp_valid(any_msg.as_alloc_resp, nlmsg_len(nlh)) )
            {
                RTSLOG_ERROR("netlink_test_recv_msg() - Got a DOLPHIN_DAEMON_ALLOC_RESP, but message was not valid, discarding. "
                        "Size was %u\n", nlmsg_len(nlh));
                atomic_set(&g_dolphin_init_success, -1);
                return;
            }

            if ( any_msg.as_alloc_resp->status != DOLPHIN_ERROR_OK )
            {
                RTSLOG_ERROR("netlink_test_recv_msg() - An error (%d) was returned by the dolphin daemon. Failed to init dolphin.\n", any_msg.as_alloc_resp->status);
                atomic_set(&g_dolphin_init_success, -1);
                return;
            }

            if( any_msg.as_alloc_resp->num_addrs != 1 )
            {
                RTSLOG_ERROR("netlink_test_recv_msg() - The number of returned addrs was %u, "
                             "but we requested %u segments.\n",
                             any_msg.as_alloc_resp->num_addrs,
                             1);
                atomic_set(&g_dolphin_init_success, -1);
                return;
            }

            //Loop over response and store pointers 
            //TODO: We ASSUME the order returned is the same as the order requested, this is correct, but we might want to return the adapter num in the future
            char * read_addr_tmp;
            char * write_addr_tmp;
            int offset;
            for(int i=0; i < any_msg.as_alloc_resp->num_addrs; ++i)
            {
                read_addr_tmp = (char*)any_msg.as_alloc_resp->addrs[ i ].read_addr;
                write_addr_tmp = (char*)any_msg.as_alloc_resp->addrs[ i ].write_addr;

                if(RFM_NUM == 0) offset = IPC_PCIE_BASE_OFFSET + RFM0_OFFSET;
                else offset = IPC_PCIE_BASE_OFFSET + RFM1_OFFSET;

                g_dolphin_read_addr = (CDS_IPC_COMMS *)(read_addr_tmp + offset);
                g_dolphin_write_addr = (CDS_IPC_COMMS *)(write_addr_tmp + offset);
            }

        break;

        default:
            RTSLOG_ERROR("netlink_test_recv_msg() - Got a message with ID %u, which we don't support, discarding.\n", any_msg.as_hdr->msg_id);
            atomic_set(&g_dolphin_init_success, -1);
            return;
        break;

    }
    atomic_set(&g_dolphin_init_success, 1);//Signal init was successful

}


int init_dolphin( void ) 
{
    struct sk_buff *skb_out;
    struct nlmsghdr *nlh;
    int res;
    dolphin_mc_alloc_req * alloc_req;

    struct netlink_kernel_cfg cfg = {
        .input = netlink_recv_resp,
    };

    g_nl_sock = netlink_kernel_create(&init_net, DOLPHIN_DAEMON_REQ_LINK_ID, &cfg);
    if (!g_nl_sock) {
        RTSLOG_ERROR("init_dolphin() - Error creating netlink socket.\n");
        return -1;
    }


    //Start message creation
    unsigned total_payload_sz = GET_ALLOC_REQ_SZ( 1 );

    ///Allocate the request's msg space, OS does free once it is sent
    skb_out = nlmsg_new( total_payload_sz, 0);
    if (!skb_out) {
        RTSLOG_ERROR("init_dolphin() - Failed to allocate skb for allocation request. Not sending...\n");
        return -1;
    }

    //Fill allocation request
    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, total_payload_sz, 0);
    alloc_req = (dolphin_mc_alloc_req*) nlmsg_data(nlh);
    alloc_req->header.msg_id = DOLPHIN_DAEMON_ALLOC_REQ;
    alloc_req->header.preamble_seq = DOLPHIN_DAEMON_MSG_PREAMBLE;
    alloc_req->num_segments = 1;
    //The RFMX uses the same segment ID across networks for RFM IPCs
    alloc_req->segments[0].segment_id = SEGMENT_ID;
    //Each connection uses a diffrent dolphin adapter, and we bridge the network across them
    alloc_req->segments[0].adapter_num = ADAPTER_NUM;
    alloc_req->segments[0].segment_sz_bytes = IPC_TOTAL_ALLOC_SIZE;

    res = nlmsg_multicast(g_nl_sock, skb_out, 0, DOLPHIN_DAEMON_REQ_GRP_ID, GFP_KERNEL);
    if( res != 0 )
    {
        if( res == -ESRCH)
            RTSLOG_ERROR("init_dolphin() - nlmsg_multicast() failed... Is the dolphin_daemon running?\n");
        else
            RTSLOG_ERROR("init_dolphin() - nlmsg_multicast() returned an error: %d\n", res);
        return -1;
    }

    //We wait for response to be handled by netlink_recv_resp()
    int time_waited_ms = 0;
    while( atomic_read(&g_dolphin_init_success) == 0 && time_waited_ms < DAEMON_RESP_TIMEOUT_MS)
    {
        msleep( DAEMON_RESP_CHECK_RATE_MS );
        time_waited_ms += DAEMON_RESP_CHECK_RATE_MS;
    }

    //Timeout case
    if(atomic_read(&g_dolphin_init_success) == 0)
    {
        RTSLOG_ERROR("init_dolphin() - Did not get a response from the dolphin_daemon in a timely manner.");
        return -1;
    }

    //If there was a returned error
    if(atomic_read(&g_dolphin_init_success) < 0)
    {
        RTSLOG_ERROR("init_dolphin() - dolphin_proxy_km returned an error, check dmesg output for error message\n");
        return -1;
    }

    //Otherwise globals are set and we should be good to go
    g_dolphin_bad_init = 0;
    return 0;


}

// ************************************************************************
void finish_dolphin( void ) 
{
    struct sk_buff *skb_out;
    struct nlmsghdr *nlh;
    int res;
    dolphin_mc_free_all_req * free_req;

    unsigned total_payload_sz = sizeof( dolphin_mc_free_all_req );

    if( g_nl_sock == NULL) return; //If we never set up the socket, just return

    //When this is set the dolphin_init failed, so we don't send the cleanup message
    if( g_dolphin_bad_init)
    {
        netlink_kernel_release(g_nl_sock);
        return;
    }


    //Allocate the request's msg space, OS does free once it's sent
    skb_out = nlmsg_new( total_payload_sz, 0);
    if (!skb_out) {
        RTSLOG_ERROR("finish_dolphin() - Failed to allocate skb for free request. Not sending...\n");
        return;
    }

    //Allocate and send message
    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, total_payload_sz, 0);


    free_req = (dolphin_mc_free_all_req*) nlmsg_data(nlh);
    free_req->header.msg_id = DOLPHIN_DAEMON_FREE_REQ;
    free_req->header.preamble_seq = DOLPHIN_DAEMON_MSG_PREAMBLE;

    res = nlmsg_multicast(g_nl_sock, skb_out, 0, DOLPHIN_DAEMON_REQ_GRP_ID, GFP_KERNEL);
    if( res != 0 )
        RTSLOG_ERROR("finish_dolphin() - nlmsg_multicast() returned an error: %d\n", res);

    netlink_kernel_release(g_nl_sock);
}

// ************************************************************************
// ************************************************************************
// Switching code communicates info with user space via a /sys/kernel file.
// The following routines provide the necessary file functions.
// ************************************************************************

/*
static ssize_t cdsrfm_show(struct kobject *kobj, struct kobj_attribute *attr,
                      char *buf)
{
    return sprintf(buf,
                   "%ld %ld %ld %ld %ld %d %d %d %d %d %d %d %d %d %d %d %d %d"
                   " %llu %llu %llu %llu\n",
                   mycounter[0], mycounter[1], mycounter[2], mycounter[3], mycounter[9],
                   myactive[0][0],myactive[0][1],myactive[0][2],
                   myactive[1][0],myactive[1][1],myactive[1][2],
                   myactive[2][0],myactive[2][1],myactive[2][2],
                   myactive[3][0],myactive[3][1],myactive[3][2], 
                   mysysstatus,
                   max_copy_times_ns[0], max_copy_times_ns[1],
                   max_copy_times_ns[2], max_copy_times_ns[3]
                   );
}

static ssize_t cdsrfm_store(struct kobject *kobj, struct kobj_attribute *attr,
                      const char *buf, size_t count)
{
        return 0;
}
*/

static int __init lr_switch_init(void)
{

    ISOLATOR_ERROR ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN];
   
    if(RFM_NUM != 0 && RFM_NUM != 1)
    {
        RTSLOG_ERROR("init: RFM_NUM not valid.\n");
	return -1;
    }

    // Initialize Dolphin NICs and get data pointers.
    if ( init_dolphin() != 0 )
    {
        RTSLOG_ERROR("lr_switch_init() - Dolphin init failed, km exiting...\n");
        finish_dolphin();
        return -ENXIO;
    }
    // Reset variable used to stop data xfer threads on rmmod.
    stop_working_threads = 0;
 
    ret = rts_isolator_run( rt_loop, -1, &g_core_used);
    if( ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(ret, error_msg);
        RTSLOG_ERROR(": rts-cpu-isolator : %s\n", error_msg);
        return -1;
    }


    return 0;
}

static void __exit lr_switch_exit(void)
{
    ISOLATOR_ERROR ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN];
    RTSLOG_INFO("Goodbye, cdsrfmswitch is shutting down\n");

    stop_working_threads = 1;

    while (atomic_read(&g_atom_has_exited) == 0)
    {
        msleep( 1 );
    }
    RTSLOG_INFO("Copy thread %d exited, bringing CPU back up...\n", g_core_used);

    ret = rts_isolator_cleanup( g_core_used );
    if( ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(ret, error_msg);
        RTSLOG_ERROR(": rts-cpu-isolator : There was an error when calling rts_isolator_cleanup(): %s\n", error_msg);
        return;
    }


    // Cleanup the dolphin NIC connections
    finish_dolphin();
}

module_init(lr_switch_init);
module_exit(lr_switch_exit);

MODULE_LICENSE("Dual BSD/GPL");
